<?php

namespace Mercans\Assessment;

use Phalcon\Validation\Validator\InclusionIn;

class AddressObjectChecker extends ObjectChecker
{
    public function __construct($obj)
    {
        parent::__construct($obj);

        $this->addValidator('country', new InclusionIn([
            'domain' => ['EE', 'US'],
        ]));
    }
}
