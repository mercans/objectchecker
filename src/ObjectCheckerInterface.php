<?php

namespace Mercans\Assessment;

use \Phalcon\Validation\Validator;

interface ObjectCheckerInterface
{
    /**
     * @param object $obj
     */
    public function __constructor($obj);

    /**
     * @param string $field
     * @param \Phalcon\Validation\Validator $validator
     */
    public function addValidator($field, Validator $validator);

    /**
     * @param string $field
     * @param string $checkerClass
     */
    public function registerObject($field, $checkerClass);

    /**
     * @return bool
     */
    public function isValid();

    /**
     * @return \Phalcon\Validation\Message\Group
     */
    public function getMessages();
}
